package main

import (
	"testing"
)

func TestFileSize_UnmarshalYAML(t *testing.T) {
	type args struct {
		rawValue string
	}
	tests := []struct {
		name    string
		fs      *FileSize
		args    args
		wantErr bool
	}{
		{
			name: "should fail for invalid number",
			fs:   new(FileSize),
			args: args{
				rawValue: "as",
			},
			wantErr: true,
		},
		{
			name: "should fail for invalid unit",
			fs:   new(FileSize),
			args: args{
				rawValue: "10 ps",
			},
			wantErr: true,
		},
		{
			name: "should succeed for positive number",
			fs:   new(FileSize),
			args: args{
				rawValue: "10",
			},
			wantErr: false,
		},
		{
			name: "should succeed for positive negative",
			fs:   new(FileSize),
			args: args{
				rawValue: "-10",
			},
			wantErr: false,
		},
		{
			name: "should succeed for positive value with unit",
			fs:   new(FileSize),
			args: args{
				rawValue: "10Kb",
			},
			wantErr: false,
		},
		{
			name: "should succeed for negative value with unit",
			fs:   new(FileSize),
			args: args{
				rawValue: "-10Kb",
			},
			wantErr: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			unmarshal := func(rIp interface{}) (err error) {
				cast := rIp.(*interface{})
				*cast = tt.args.rawValue
				return nil
			}
			if err := tt.fs.UnmarshalYAML(unmarshal); (err != nil) != tt.wantErr {
				t.Errorf("FileSize.UnmarshalYAML() error = %v, wantErr %v", err, tt.wantErr)
			}
		})
	}
}
