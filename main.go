package main

import (
	"fmt"
	"os"
	"os/signal"
	"path"
	"path/filepath"
	"strings"
	"syscall"
	"time"

	"github.com/kennygrant/sanitize"
	"github.com/mmcdole/gofeed"
	"go.uber.org/zap"
)

var log *zap.SugaredLogger

func main() {
	cmdOpts := parseCmdOptions()
	initLogger(cmdOpts)
	defer log.Sync()
	config := readConfigurationFile(cmdOpts.ConfigFile)
	config.DeamonMode = cmdOpts.DeamonMode

	if cmdOpts.ValidateConfig {
		if err := validateConfig(config); err != nil {
			log.Fatalf(err.Error())
		}
		fmt.Printf("Configuration file %s is valid\n", cmdOpts.ConfigFile)
		printConfig(config)
		os.Exit(0)
	}
	if !config.DeamonMode {
		executeFeeds(&config)
		writeConfig(cmdOpts.ConfigFile, config)
		return
	}
	cancelChan := make(chan os.Signal, 1)
	// catch SIGTERM or SIGINTERRUPT
	signal.Notify(cancelChan, syscall.SIGTERM, syscall.SIGINT)
	go func() {
		log.Info("Daemon started")
		for {
			log.Debug("Refreshing Feeds")
			executeFeeds(&config)

			log.Debug("Refresh Done")
			time.Sleep(config.RefreshRate)
		}
	}()
	sig := <-cancelChan
	log.Debugf("Caught SIGTERM %v", sig)
	log.Info("Shutting down")
	writeConfig(cmdOpts.ConfigFile, config)
}

func initLogger(cmdOpts cmdOpts) {
	var logger *zap.Logger
	var err error
	if cmdOpts.Debug {
		logger, err = zap.NewDevelopment()
	} else {
		config := zap.NewProductionConfig()
		config.Encoding = "console"
		logger, err = config.Build()
	}
	if err != nil {
		panic(err)
	}
	log = logger.Sugar()
}

func executeFeeds(config *config) {
	client := NewDownloadClient(config.ClientConfiguration)
	for k, v := range config.Feeds {
		log.Infof("Downloading feed %s", k)
		time, err := processFeed(k, v, client)
		if err != nil {
			if e, ok := err.(*NoUpdateError); ok {
				log.Error(e.Error())
			} else {
				log.Panic(err)
			}
		}
		v.lastUpdated = time
		config.Feeds[k] = v
	}
}

func processFeed(feedName string, feed feed, client DownloadClient) (time.Time, error) {
	log.Debugf("%+v\n", feed)
	fp := gofeed.NewParser()
	feedInfo, err := fp.ParseURL(feed.URL)
	if err != nil {
		return time.Time{}, err
	}
	if len(feedInfo.Items) == 0 {
		return time.Time{}, &NoUpdateError{FeedName: feedName}
	}
	for _, v := range feedInfo.Items {
		if v.PublishedParsed.Before(feed.lastUpdated) {
			log.Infof("[%s] - %s skipping", feedName, v.Title)
			continue
		}
		filename := extractFilename(*v, feed.FileExtension)

		filepath := path.Join(feed.TorrentDirectory, filename)
		log.Debugw("downloading feed item", "location", filepath)
		err := DownloadFile(filepath, v.Link)
		if err != nil {
			log.Error(DownloadFailedError{FeedName: feedName, URL: v.Link})
			continue
		}
		log.Infof("[%s] - %s downloaded", feedName, v.Title)
		err = addToClient(
			filepath,
			feed.Labels,
			client,
		)
		if err != nil {
			log.Errorf("[%s] - %s failed to add to download client. error: %v", feedName, v.Title, err)
		}
		err = MakeItemRead(v.GUID)
		if err != nil {
			log.Warnf("[%s] - %s failed to mark as read", feedName, v.Title)
		}
	}
	lastFeed := feedInfo.Items[len(feedInfo.Items)-1]
	return *lastFeed.PublishedParsed, nil
}

func addToClient(filepath string, labels []string, client DownloadClient) error {
	if client == nil {
		return nil
	}
	err := client.AddFileToClient(filepath, labels...)
	if err != nil {
		return err
	}
	err = os.Remove(filepath)
	if err != nil {
		log.Infof("Could not delete file %s", filepath)
	}
	return nil
}

func extractFilename(feedInfo gofeed.Item, fileExt string) string {
	filename, err := FileNameFromContentDisposition(feedInfo.GUID)
	if err != nil {
		log.Debug("Setting feed title as filename")
		filename = feedInfo.Title
	}
	ext := filepath.Ext(filename)
	base := strings.TrimSuffix(filename, ext)

	if ext == "" && fileExt != "" {
		ext = fileExt
	}
	return sanitize.BaseName(base) + ext
}

//  LocalWords:  SIGINTERRUPT SIGETRM
