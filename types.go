package main

import (
	"fmt"
	"reflect"
	"regexp"
	"strconv"
	"time"
)

type config struct {
	DeamonMode          bool                `yaml:"deamon-mode"`
	RefreshRate         time.Duration       `yaml:"refresh,omitempty"`
	ClientConfiguration ClientConfiguration `yaml:"clientConfiguration,omitempty"`
	Feeds               map[string]feed     `yaml:"feeds,omitempty"`
	commonConfig        `yaml:",inline"`
}

type feed struct {
	URL           string `yaml:"url"`
	REI           string `yaml:"include,omitempty"`
	REX           string `yaml:"exclude,omitempty"`
	FileExtension string `yaml:"file-extension,omitempty"`
	commonConfig  `yaml:",inline"`
	lastUpdated   time.Time `yaml:"lastUpdated,omitempty"`
}

type commonConfig struct {
	Labels           []string `yaml:"labels,omitempty"`
	TorrentDirectory string   `yaml:"torrentDirectory,omitempty"`
	MinSize          FileSize `yaml:"min-size,omitempty"`
	MaxSize          FileSize `yaml:"max-size,omitempty"`
}

type cmdOpts struct {
	ConfigFile     string `short:"c" long:"conf" description:"configuration file path" default:"$HOME/.config/rss-godler"`
	DeamonMode     bool   `short:"d" long:"deamon-mode" description:"run in a deamon mode"`
	Debug          bool   `long:"debug" description:"Enabled debug logging"`
	ValidateConfig bool   `long:"validate-config" description:"validate configuration file and exit."`
	PrintVersion   bool   `short:"v" long:"version" group:"other" description:"Display version number"`
}

type feedTracker struct {
	Feeds map[string]trackingInfo `yaml:"feeds,flow"`
}

type trackingInfo struct {
	lastUpdated time.Time `yaml:"lastUpdated"`
}

type ClientConfiguration struct {
	DownloadClient            string                    `yaml:"downloadClient,omitempty"`
	TransmissionConfiguration TransmissionConfiguration `yaml:"transmissionConfiguration,omitempty"`
	Labels                    []string                  `yaml:"labels,omitempty"`
	DownloadDirectory         string                    `yaml:"downloadDirectory,omitempty"`
}

type TransmissionConfiguration struct {
	User     string `yaml:"user,omitempty"`
	Password string `yaml:"password,omitempty"`
	Host     string `yaml:"host,omitempty"`
	Port     string `yaml:"port,omitempty"`
	UseTls   bool   `yaml:"useTls,omitempty"`
}

const (
	//Byte 8 bits
	Byte FileSize = 1
	//Kibibyte 1024 Bytes
	Kibibyte = 1024 * Byte
	// Mebibyte 1024 KiloBytes
	Mebibyte = 1024 * Kibibyte
	// Gibibyte 1024 Mebibytes
	Gibibyte = 1024 * Mebibyte
	//Tebibyte 1024 Gibibyte
	Tebibyte = 1024 * Gibibyte

	//Kilobyte 1000 Bytes
	Kilobyte = 1000 * Byte
	// Megabyte 1000 KiloBytes
	Megabyte = 1000 * Kilobyte
	// Gigabyte 1000 Megabyte
	Gigabyte = 1000 * Megabyte
	//Terabyte 1000 Gigabyte
	Terabyte = 1000 * Gigabyte
)

// FileSize represents number of bytes
type FileSize int64

var (
	//SizeRegEx determines given string is valid size declaratio
	SizeRegEx = regexp.MustCompile(`^-?(?P<value>[[:digit:]]+)[ ]?(?P<unit>(b|B|Kb|Kib|Mb|Mib|Gb|Gib|Tb|Tib)?)$`)
)

// UnmarshalYAML Implements the Unmarshaler interface of the yaml pkg.
func (fs *FileSize) UnmarshalYAML(unmarshal func(interface{}) error) error {
	var iRp interface{}
	var rawValue string
	err := unmarshal(&iRp)
	if err != nil {
		return err
	}
	iRpType := reflect.TypeOf(iRp)
	if iRpType.AssignableTo(reflect.TypeOf("")) {
		rawValue = iRp.(string)
	} else if iRpType.AssignableTo(reflect.TypeOf(int64(0))) {
		rawValue = fmt.Sprint(iRp.(int64))
	}
	if rawValue == "None" || rawValue == "" {
		*fs = FileSize(-1)
		return nil
	}
	if !SizeRegEx.MatchString(rawValue) {
		return fmt.Errorf("value must be digits optionally followed by one of the units (b, B, Kb, Kib, Mb, Mib, Gb, Gib, Tb, Tib) without any spaces")
	}
	unit := SizeRegEx.ReplaceAllString(rawValue, "${unit}")
	value, err := strconv.ParseFloat(SizeRegEx.ReplaceAllString(rawValue, "${value}"), 64)
	if err != nil {
		return err
	}
	bytes := func(unit string, value float64) float64 {
		switch unit {
		case "Kb":
			return value * float64(Kilobyte)
		case "Kib":
			return value * float64(Kibibyte)
		case "Mb":
			return value * float64(Megabyte)
		case "Mib":
			return value * float64(Mebibyte)
		case "Gb":
			return value * float64(Gigabyte)
		case "Gib":
			return value * float64(Gibibyte)
		case "Tb":
			return value * float64(Terabyte)
		case "Tib":
			return value * float64(Tebibyte)
		case "b":
			fallthrough
		case "B":
			fallthrough
		default:
			return value
		}
	}(unit, value)

	*fs = FileSize(bytes)
	return nil
}

// NoUpdateError defines error where feed has no updates available
type NoUpdateError struct {
	FeedName string
}

func (e *NoUpdateError) Error() string {
	return e.FeedName + " has no updates"
}

// DownloadFailedError defines error whule downloading feed item
type DownloadFailedError struct {
	FeedName string
	URL      string
}

func (e *DownloadFailedError) Error() string {
	return "[" + e.FeedName + "]" + " could not download item " + e.URL
}
