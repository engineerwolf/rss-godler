package main

import (
	"encoding/json"
	"fmt"
	"os"
	"regexp"
	"time"

	"io/fs"

	flags "github.com/jessevdk/go-flags"
	yaml "gopkg.in/yaml.v2"
)

var (
	version = "undefined"
)

func parseCmdOptions() (cmdOpts cmdOpts) {
	_, err := flags.Parse(&cmdOpts)

	if err != nil {
		if _, ok := err.(*flags.Error); ok {
			os.Exit(1)
		} else {
			log.Fatalf(err.Error())
		}
	}
	if cmdOpts.PrintVersion {
		fmt.Printf("rss-godler v.%s\n", version)
		os.Exit(0)
	}
	return
}

func readConfigurationFile(filename string) (config config) {
	data, err := os.ReadFile(filename)
	if err != nil {
		log.Fatalf("Failed to open file %s", filename)
	}
	err2 := yaml.Unmarshal(data, &config)
	if err2 != nil {
		log.Fatalf("cannot unmarshal data: %v", err2)
	}
	setDefaults(&config)
	for k, feed := range config.Feeds {
		config.Feeds[k] = populateDefaults(feed, config.commonConfig)
	}
	return
}

func writeConfig(filename string, config config) {
	data, err := yaml.Marshal(config)
	if err != nil {
		log.Panic(err)
	}
	err = os.WriteFile(filename, data, fs.FileMode(0644))
	if err != nil {
		log.Panic(err)
	}
}

func populateDefaults(feed feed, commonConfig commonConfig) feed {
	if feed.TorrentDirectory == "" {
		fmt.Println("updating torrent dir")
		feed.TorrentDirectory = commonConfig.TorrentDirectory
	}
	if feed.MinSize == 0 {
		feed.MinSize = commonConfig.MinSize
	}
	if feed.MaxSize == 0 {
		feed.MaxSize = commonConfig.MaxSize
	}
	if len(feed.Labels) == 0 {
		feed.Labels = []string{}
	}
	feed.Labels = append(feed.Labels, commonConfig.Labels...)
	return feed
}

func setDefaults(config *config) {
	if config.TorrentDirectory == "" {
		config.TorrentDirectory = "./"
	}
	if config.RefreshRate < 0 {
		config.RefreshRate = 15 * time.Minute
	}
	if config.RefreshRate < 1*time.Minute {
		config.RefreshRate = 15 * time.Minute
	}
	if config.MaxSize == 0 {
		config.MaxSize = -1
	}
}

func validateConfig(config config) error {

	for k, v := range config.Feeds {
		if v.REI != "" {
			if _, err := regexp.Compile(v.REI); err != nil {
				return fmt.Errorf("Include expression for %s is invalid. %s", k, v.REI)
			}
		}
		if v.REX != "" {
			if _, err := regexp.Compile(v.REX); err != nil {
				return fmt.Errorf("Exclude expression for %s is invalid. %s", k, v.REX)
			}
		}
	}
	return nil
}

func printConfig(config config) {
	c, _ := json.MarshalIndent(config, "", "  ")
	fmt.Println(string(c))
}
