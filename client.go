package main

import "fmt"

func NewDownloadClient(clc ClientConfiguration) DownloadClient {
	switch clc.DownloadClient {
	case "transmission":
		return NewTransmissionClient(clc)
	case "":
		return nil
	default:
		panic(fmt.Sprintf("Not supported value [%s] for DownloadClient", clc.DownloadClient))
	}
}
