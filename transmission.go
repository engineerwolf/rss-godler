package main

import (
	"context"
	"fmt"
	"net/url"

	"github.com/hekmon/transmissionrpc/v3"
)

type TransmissionClient struct {
	tbt          *transmissionrpc.Client
	labels       []string
	downloadPath string
}

func NewTransmissionClient(clc ClientConfiguration) DownloadClient {
	client := initializeTransmissionClient(clc.TransmissionConfiguration)
	return &TransmissionClient{
		tbt:          client,
		labels:       clc.Labels,
		downloadPath: clc.DownloadDirectory,
	}
}

func (c TransmissionClient) AddFileToClient(filepath string, labels ...string) error {
	b64, err := transmissionrpc.File2Base64(filepath)
	if err != nil {
		return fmt.Errorf("can't encode '%s' context as base64: %v", filepath, err)
	}
	payload := transmissionrpc.TorrentAddPayload{
		MetaInfo: &b64,
	}
	return c.addToClient(payload, labels)
}

func (c TransmissionClient) AddMagnetToClient(magnet string, labels ...string) error {
	payload := transmissionrpc.TorrentAddPayload{
		Filename: &magnet,
	}
	return c.addToClient(payload, labels)
}

func (c TransmissionClient) addToClient(payload transmissionrpc.TorrentAddPayload, labels []string) error {
	pause := false
	payload.Paused = &pause
	payload.DownloadDir = &c.downloadPath
	payload.Labels = labels

	_, err := c.tbt.TorrentAdd(context.TODO(), payload)
	if err != nil {
		return err
	}
	return nil

}

func initializeTransmissionClient(trc TransmissionConfiguration) *transmissionrpc.Client {
	trcUrl := fmt.Sprintf("%s:%s@%s:%s/transmission/rpc",
		trc.User,
		trc.Password,
		trc.Host,
		trc.Port)
	if trc.UseTls {
		trcUrl = "https://" + trcUrl
	} else {
		trcUrl = "http://" + trcUrl
	}
	endpoint, err := url.Parse(trcUrl)
	if err != nil {
		panic(err)
	}
	tbt, err := transmissionrpc.New(endpoint, nil)
	if err != nil {
		panic(err)
	}
	ok, serverVersion, serverMinimumVersion, err := tbt.RPCVersion(context.TODO())
	if err != nil {
		panic(err)
	}
	if !ok {
		panic(fmt.Sprintf("Remote transmission RPC version (v%d) is incompatible with the transmission library (v%d): remote needs at least v%d",
			serverVersion, transmissionrpc.RPCVersion, serverMinimumVersion))
	}
	fmt.Printf("Remote transmission RPC version (v%d) is compatible with our transmissionrpc library (v%d)\n",
		serverVersion, transmissionrpc.RPCVersion)
	return tbt
}
