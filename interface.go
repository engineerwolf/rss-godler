package main

type DownloadClient interface {
	AddFileToClient(filepath string, labels ...string) error
	AddMagnetToClient(magnet string, labels ...string) error
}
