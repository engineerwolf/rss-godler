module github.com/engineerwolf/rss-godler

go 1.22

require (
	github.com/hekmon/transmissionrpc/v3 v3.0.0
	github.com/jessevdk/go-flags v1.4.0
	github.com/kennygrant/sanitize v1.2.4
	github.com/mmcdole/gofeed v1.0.0-beta2
	github.com/stretchr/testify v1.4.0
	go.uber.org/zap v1.15.0
	gopkg.in/yaml.v2 v2.2.2
)

require (
	github.com/PuerkitoBio/goquery v1.5.0 // indirect
	github.com/andybalholm/cascadia v1.0.0 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/hashicorp/go-cleanhttp v0.5.2 // indirect
	github.com/hekmon/cunits/v2 v2.1.0 // indirect
	github.com/mmcdole/goxpp v0.0.0-20181012175147-0068e33feabf // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	go.uber.org/atomic v1.6.0 // indirect
	go.uber.org/multierr v1.5.0 // indirect
	golang.org/x/net v0.0.0-20190620200207-3b0461eec859 // indirect
	golang.org/x/text v0.3.0 // indirect
)
