package main

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func FTestAddToClientTorrent(t *testing.T) {
	file := "../testdata/ubuntu-24.04-live-server-amd64.iso.torrent"
	assert.FileExists(t, file)
	client := NewTransmissionClient(ClientConfiguration{
		DownloadDirectory: "/complete/anime/",
		Labels:            []string{"anime"},
		DownloadClient:    "transmission",
		TransmissionConfiguration: TransmissionConfiguration{
			UseTls:   false,
			User:     "",
			Password: "",
			Port:     "13371",
			Host:     "0.0.0.0",
		},
	})
	assert.NotNil(t, client)
	client.AddFileToClient(file, "", "anime")
}

func FTestAddToClientMagnetFile(t *testing.T) {
	magnet := "magnet:?xt=urn:btih:d984f67af9917b214cd8b6048ab5624c7df6a07a&tr=https%3A%2F%2Facademictorrents.com%2Fannounce.php&tr=udp%3A%2F%2Ftracker.coppersurfer.tk%3A6969&tr=udp%3A%2F%2Ftracker.opentrackr.org%3A1337%2Fannounce"
	client := NewTransmissionClient(ClientConfiguration{
		DownloadDirectory: "/complete/anime/",
		Labels:            []string{"anime"},
		DownloadClient:    "transmission",
		TransmissionConfiguration: TransmissionConfiguration{
			UseTls:   false,
			User:     "",
			Password: "",
			Port:     "13371",
			Host:     "0.0.0.0",
		},
	})
	assert.NotNil(t, client)
	client.AddMagnetToClient(magnet, "", "anime")
}
