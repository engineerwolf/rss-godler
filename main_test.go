package main

import (
	"net/http"
	"net/http/httptest"
	"testing"

	"github.com/mmcdole/gofeed"
)

func Test_extractAtom(t *testing.T) {
	initLogger(cmdOpts{Debug: true})
	type setupArgs struct {
		Title              string
		ContentDisposition string
	}
	type args struct {
		fileExt string
	}
	tests := []struct {
		name  string
		setup setupArgs
		args  args
		want  string
	}{
		{
			name: "missing Content-disposition, Title Without Ext",
			setup: setupArgs{
				Title: "Sometitle",
			},
			args: args{
				fileExt: ".torrent",
			},
			want: "Sometitle.torrent",
		},
		{
			name: "missing Content-disposition, Title With Ext",
			setup: setupArgs{
				Title: "Sometitle.tor",
			},
			args: args{
				fileExt: ".torrent",
			},
			want: "Sometitle.tor",
		},
		{
			name: "missing Content-disposition, Title with same Ext",
			setup: setupArgs{
				Title: "Sometitle.torrent",
			},
			args: args{
				fileExt: ".torrent",
			},
			want: "Sometitle.torrent",
		},
		{
			name: "with Content-disposition and Ext",
			setup: setupArgs{
				Title:              "Sometitle",
				ContentDisposition: `inline; filename="%5BDeadFish%5D%20Sakamoto%20desu%20ga%3F%20-%2001v2%20%5B720p%5D%5BAAC%5D.mp4.torrent"; filename*=UTF-8''%5BDeadFish%5D%20Sakamoto%20desu%20ga%3F%20-%2001v2%20%5B720p%5D%5BAAC%5D.mp4.torrent`,
			},
			args: args{
				fileExt: "torrent",
			},
			want: "DeadFish-Sakamoto-desu-ga-01v2-720pAAC-mp4.torrent",
		},
		{
			name: "with Content-disposition and without Ext",
			setup: setupArgs{
				Title:              "Sometitle",
				ContentDisposition: `inline; filename="%5BDeadFish%5D%20Sakamoto%20desu%20ga%3F%20-%2001v2%20%5B720p%5D%5BAAC%5D"; filename*=UTF-8''%5BDeadFish%5D%20Sakamoto%20desu%20ga%3F%20-%2001v2%20%5B720p%5D%5BAAC%5D`,
			},
			args: args{
				fileExt: ".torrent",
			},
			want: "DeadFish-Sakamoto-desu-ga-01v2-720pAAC.torrent",
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			server := httptest.NewServer(http.HandlerFunc(func(w http.ResponseWriter, r *http.Request) {
				w.Header().Add("Content-Disposition", tt.setup.ContentDisposition)
				w.WriteHeader(200)
				w.Write([]byte{})
			}))
			feedInfo := gofeed.Item{
				Title: tt.setup.Title,
				GUID:  server.URL,
			}
			got := extractFilename(feedInfo, tt.args.fileExt)
			if got != tt.want {
				t.Errorf("extractAtom() got = %v, want %v", got, tt.want)
			}
			server.Close()
		})
	}
}
